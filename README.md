# Anywhere-Health

## yarnがインストールされていない
```
$ brew install yarn
```

## rbenvのインストール
```
$ git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
```
~/.bash_profileをに以下を追記。
```
export PATH=$HOME/.rbenv/bin:$PATH
eval "$(rbenv init -)"
```
バージョン確認
```
$ rbenv --version
```

## ruby-buildのインストール
```
$ git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
```
インストール可能なバージョン一覧表示
```
$ rbenv install -l
```
バージョン「2.7.1」のrubyをインストール
```
$ rbenv install 2.7.1
```
インストールされたか確認
```
$ rbenv versions
```
使用可能なバージョンに変更
```
$ rbenv global 2.7.1
```
バージョンが切り替わったか確認
```
$ ruby --version
```

## Railsのインストール
```
$ gem install bundler
$ bundle install
```

## 利用gem,ツールのインストール
```
$ yarn install --check-files
$ gem pristine nokogiri --version 1.10.9
```

## MySQLのインストール
```
$ 
```







